Name:           gphoto2
Version:        2.5.28
Release:        1
Summary:        Access software for digital cameras
License:        GPLv2+
Url:            http://www.gphoto.org/
Source0:        https://sourceforge.net/projects/gphoto/files/gphoto/%{version}/%{name}-%{version}.tar.bz2
Patch1:         gphoto2-2.5.17-sw.patch

BuildRequires:  make pkgconfig(libgphoto2) >= %{version} libjpeg-devel
BuildRequires:  pkgconfig(libexif) popt-devel readline-devel gcc

%description
The gPhoto2 project is a universal free application and library framework that allows you to download
images from several different digital camera models, including newer models with USB connections.

%package        help
Summary:        Documentation for gphoto2

%description    help
This package provides documentation for gphoto2.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install INSTALL="install -p"
%find_lang %{name}

%files -f %{name}.lang
%doc NEWS README TODO COPYING
%{_bindir}/gphoto2
%exclude %{_docdir}/%{name}/test-hook.sh

%files help
%{_mandir}/man1/gphoto2.1*

%changelog
* Fri Mar 1 2024 liyanan <liyanan61@h-partners.com> - 2.5.28-1
- Update to version 2.5.28

* Fri Dec 08 2023 Ge Wang <wang__ge@126.com> - 2.5.27-1
- Update to version 2.5.27

* Thu Oct 20 2022 wuzx<wuzx1226@qq.com> - 2.5.17-5
- add sw64 patch

* Mon Jun 7 2021 baizhonggui <baizhonggui@huawei.com> - 2.5.17-4
- Fix building error: configure: error: no acceptable C compiler found in $PATH
- Add gcc in BuildRequires

* Tue Dec 31 2019 fengbing <fengbing7@huawei.com> - 2.5.17-3
- Package init
